# TriggeriseBEChallenge

This Triggerise BEC hallenge exercise was made by António Dourado
The project also contains a war file with this project.
You can test this using Java EE eclipe, or you can configurate a apache tomcat and use the war file in it

Some REST requests:

GET http://localhost:8080/TriggeriseBEChallenge/catalog
Result:
{
    "result": [
        {
            "id": 1,
            "code": "TICKET",
            "name": "Triggerise Ticket",
            "price": 5
        },
        {
            "id": 2,
            "code": "HOODIE",
            "name": "Triggerise Hoodie",
            "price": 20
        },
        {
            "id": 3,
            "code": "HAT",
            "name": "Triggerise Hat",
            "price": 7.5
        }
    ],
    "success": true
}


http://localhost:8080/TriggeriseBEChallenge/items
Result:
{
    "result": [
        {
            "id": 1,
            "code": "TICKET",
            "name": "Triggerise Ticket",
            "price": 5,
            "altPrice": 0,
            "idt": "TWO_FOR_ONE"
        },
        {
            "id": 2,
            "code": "HOODIE",
            "name": "Triggerise Hoodie",
            "price": 20,
            "altPrice": 19,
            "idt": "BULK"
        },
        {
            "id": 3,
            "code": "HAT",
            "name": "Triggerise Hat",
            "price": 7.5,
            "altPrice": 0,
            "idt": "NORMAL"
        }
    ],
    "success": true
}

http://localhost:8080/TriggeriseBEChallenge/items/1
Result:
{
    "result": {
        "id": 1,
        "code": "TICKET",
        "name": "Triggerise Ticket",
        "price": 5,
        "altPrice": 0,
        "idt": "TWO_FOR_ONE"
    },
    "success": true
}



POST http://localhost:8080/TriggeriseBEChallenge/buy
Results:
1) TICKET, HOODIE, HAT
body:
[{"id": 1}, {"id": 2}, {"id": 3}]
return:
{
    "result": "Items: TICKET, HOODIE, HAT\n Total: 32.50€",
    "success": true
}


2) TICKET, HOODIE, TICKET
body:
[{"id": 1}, {"id": 2}, {"id": 1}]
return:
{
    "result": "Items: TICKET, HOODIE, TICKET\n Total: 25.00€",
    "success": true
}


3) HOODIE, HOODIE, HOODIE, TICKET, HOODIE
body:
[{"id": 2}, {"id": 2}, {"id": 2}, {"id": 1}, {"id": 2}]
return:
{
    "result": "Items: HOODIE, HOODIE, HOODIE, TICKET, HOODIE\n Total: 81.00€",
    "success": true
}


4) TICKET, HOODIE, TICKET, TICKET, HAT, HOODIE, HOODIE
body:
[{"id": 1}, {"id": 2}, {"id": 1}, {"id": 1}, {"id": 3}, {"id": 2}, {"id": 2}]
return:
{
    "result": "Items: TICKET, HOODIE, TICKET, TICKET, HAT, HOODIE, HOODIE\n Total: 74.50€",
    "success": true
}