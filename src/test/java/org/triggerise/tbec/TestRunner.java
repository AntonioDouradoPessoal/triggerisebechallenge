package org.triggerise.tbec;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.triggerise.tbec.configuration.AppConfiguration;
import org.triggerise.tbec.enumeration.item.ItemDiscountType;
import org.triggerise.tbec.model.Item;
import org.triggerise.tbec.service.store.StoreService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {AppConfiguration.class})
public class TestRunner {
	
	@Autowired
	StoreService storeService;
	
	
	@Test
	public void testProcessSaleCase1() {//TICKET, HOODIE, HAT
		String objective = "Items: TICKET, HOODIE, HAT\n Total: 32.50�";
		List<Item> imputData = new ArrayList<Item>();
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL));
		String result = storeService.processSale(imputData);
		assertEquals(objective, result);
	}
	
	@Test
	public void testProcessSaleCase2() {//TICKET, HOODIE, TICKET
		String objective = "Items: TICKET, HOODIE, TICKET\n Total: 25.00�";
		List<Item> imputData = new ArrayList<Item>();
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		String result = storeService.processSale(imputData);
		assertEquals(objective, result);
	}
	
	@Test
	public void testProcessSaleCase3() {//HOODIE, HOODIE, HOODIE, TICKET, HOODIE
		String objective = "Items: HOODIE, HOODIE, HOODIE, TICKET, HOODIE\n Total: 81.00�";
		List<Item> imputData = new ArrayList<Item>();
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		String result = storeService.processSale(imputData);
		assertEquals(objective, result);
	}
	
	@Test
	public void testProcessSaleCase4() {//TICKET, HOODIE, TICKET, TICKET, HAT, HOODIE, HOODIE
		String objective = "Items: TICKET, HOODIE, TICKET, TICKET, HAT, HOODIE, HOODIE\n Total: 74.50�";
		List<Item> imputData = new ArrayList<Item>();
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		String result = storeService.processSale(imputData);
		assertEquals(objective, result);
	}
 
}
