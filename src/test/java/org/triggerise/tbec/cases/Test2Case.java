package org.triggerise.tbec.cases;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.triggerise.tbec.enumeration.item.ItemDiscountType;
import org.triggerise.tbec.model.Item;
import org.triggerise.tbec.service.store.StoreService;

public class Test2Case {
	
	@Autowired
	private StoreService storeService;

	@Test
	public void testProcessSale() {
		String objective = "Items: TICKET, HOODIE, HAT\n Total: 32.50�";
		List<Item> imputData = new ArrayList<Item>();
		imputData.add(new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE));
		imputData.add(new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK));
		imputData.add(new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL));
		String result = storeService.processSale(imputData);
		assertEquals(objective, result);
	}

}
