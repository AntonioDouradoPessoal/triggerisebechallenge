package org.triggerise.tbec.repository;

import java.util.ArrayList;
import java.util.List;

import org.triggerise.tbec.enumeration.item.ItemDiscountType;
import org.triggerise.tbec.model.Item;

public class CatalogRepository {
		
	public CatalogRepository() {
		
	}

	public List<Item> getAll(){
		List<Item> existingCatalog = new ArrayList<Item>();
		Item item1 = new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE);
		existingCatalog.add(item1);
		Item item2 = new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK);
		existingCatalog.add(item2);
		Item item3 = new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL);
		existingCatalog.add(item3);
		return existingCatalog;
	}
	
	public Item getOne(Long id){
		List<Item> existingCatalog = new ArrayList<Item>();
		Item item1 = new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE);
		existingCatalog.add(item1);
		Item item2 = new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK);
		existingCatalog.add(item2);
		Item item3 = new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL);
		existingCatalog.add(item3);
		
		for(Item item : existingCatalog) {
	        if(item.getId().equals(id)) {
	            return item;
	        }
	    }
		return null;
	}
	
	public int countTotal(){
		List<Item> existingCatalog = new ArrayList<Item>();
		Item item1 = new Item(1L,"TICKET","Triggerise Ticket",5,0, ItemDiscountType.TWO_FOR_ONE);
		existingCatalog.add(item1);
		Item item2 = new Item(2L,"HOODIE","Triggerise Hoodie",20,19, ItemDiscountType.BULK);
		existingCatalog.add(item2);
		Item item3 = new Item(3L,"HAT","Triggerise Hat",7.5,0, ItemDiscountType.NORMAL);
		existingCatalog.add(item3);
		return existingCatalog.size();
	}
}
