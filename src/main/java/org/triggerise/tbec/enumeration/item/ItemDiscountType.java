package org.triggerise.tbec.enumeration.item;

public enum ItemDiscountType {
	NORMAL,
	TWO_FOR_ONE,
	BULK
}
