package org.triggerise.tbec.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.triggerise.tbec.dto.ItemBasic;
import org.triggerise.tbec.dto.ItemToBuyMin;
import org.triggerise.tbec.model.Item;
import org.triggerise.tbec.service.item.ItemService;
import org.triggerise.tbec.service.store.StoreService;


@RestController
public class AppRestController {
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private ItemService itemService;

	@RequestMapping(value = "/items", method = RequestMethod.GET)
	public HashMap<String, Object> getAll(){
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			List<Item> all = itemService.getAll();
			map.put("result", all);
			map.put("success", true);
			return map;
		}catch (Exception e) {
			map.put("error", e.getStackTrace());
			map.put("success", false);
			return map;
		}
	}
	
	@RequestMapping(value = "/items/{id}", method = RequestMethod.GET)
	public HashMap<String, Object> getOne(@PathVariable(value = "id") Long id){
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			Item item = itemService.getOneById(id);
			map.put("result", item);
			map.put("success", true);
			return map;
		}catch (Exception e) {
			map.put("error", e.getStackTrace());
			map.put("success", false);
			return map;
		}
	}
	
	@RequestMapping(value = "/catalog", method = RequestMethod.GET)
	public HashMap<String, Object> getAllBasic(){
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			List<ItemBasic> allBasic = new ArrayList<ItemBasic>();
			List<Item> all = itemService.getAll();
			for (Item currentCatalogItem : all) {
				ItemBasic info = itemService.parseItemToBasic(currentCatalogItem);
				allBasic.add(info);
			}
			map.put("result", allBasic);
			map.put("success", true);
			return map;
		}catch (Exception e) {
			map.put("error", e.getStackTrace());
			map.put("success", false);
			return map;
		}
	}
	
	@RequestMapping(value = "/buy", method = RequestMethod.POST)
	public HashMap<String, Object> processSale(@RequestBody List<ItemToBuyMin> itemToBuyList){
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			List<Item> shoppingCart = new ArrayList<Item>();  
			if(itemToBuyList != null && !itemToBuyList.isEmpty()){
				for (ItemToBuyMin itemToBuy : itemToBuyList) {
					Item currentCatalogItem = itemService.getOneById(itemToBuy.getId());
					if(currentCatalogItem != null){
						shoppingCart.add(currentCatalogItem);
					}
				}
				map.put("result", storeService.processSale(shoppingCart));
				map.put("success", true);
			}
			else{
				map.put("success", false);
			}
			return map;
		}catch (Exception e) {
			map.put("error", e.getStackTrace());
			map.put("success", false);
			return map;
		}
	}
  

	
}