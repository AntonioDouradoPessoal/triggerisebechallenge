package org.triggerise.tbec.dto;

public class ItemBasic {

	private Long id;
	private String code;
	private String name;
	private double price;
	
	public ItemBasic() {
		
	}

	public ItemBasic(Long id, String code, String name, double price) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}