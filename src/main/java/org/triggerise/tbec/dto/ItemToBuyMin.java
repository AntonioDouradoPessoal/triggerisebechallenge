package org.triggerise.tbec.dto;

public class ItemToBuyMin {

	private Long id;
	public ItemToBuyMin() {
		
	}

	public ItemToBuyMin(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}