package org.triggerise.tbec.model;

import org.triggerise.tbec.enumeration.item.ItemDiscountType;

public class Item {

	private Long id;
	private String code;
	private String name;
	private double price;
	private double altPrice;
	private ItemDiscountType idt;
	
	public Item() {
		
	}

	public Item(Long id, String code, String name, double price, double altPrice, ItemDiscountType idt) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.price = price;
		this.altPrice = altPrice;
		this.idt = idt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAltPrice() {
		return altPrice;
	}

	public void setAltPrice(double altPrice) {
		this.altPrice = altPrice;
	}

	public ItemDiscountType getIdt() {
		return idt;
	}

	public void setIdt(ItemDiscountType idt) {
		this.idt = idt;
	}
	
	public int compareId(Item item) {
		return Double.compare(getId(), item.getId());
    }
}