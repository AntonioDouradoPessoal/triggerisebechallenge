package org.triggerise.tbec.service.item.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.triggerise.tbec.dto.ItemBasic;
import org.triggerise.tbec.model.Item;
import org.triggerise.tbec.service.item.ItemService;
import org.triggerise.tbec.repository.CatalogRepository;

@Service("ItemService")
public class ItemServiceImpl implements ItemService{
		
	public List<Item> getAll() {
		CatalogRepository cr  = new CatalogRepository();
		return cr.getAll();
	}

	public Item getOneById(Long id) {
		CatalogRepository cr  = new CatalogRepository();
		return cr.getOne(id);
	}

	public int countTotal() {
		CatalogRepository cr  = new CatalogRepository();
		return cr.countTotal();
	}
	
	public ItemBasic parseItemToBasic(Item item){
		ItemBasic itemToBuyFull = new ItemBasic();
		itemToBuyFull.setId(item.getId());
		itemToBuyFull.setCode(item.getCode());
		itemToBuyFull.setName(item.getName());
		itemToBuyFull.setPrice(item.getPrice());
		return itemToBuyFull;
	}

} 