package org.triggerise.tbec.service.item;

import java.util.List;

import org.triggerise.tbec.dto.ItemBasic;
import org.triggerise.tbec.model.Item;

public interface ItemService {
	public List<Item> getAll();
	public Item getOneById(Long id);
	public int countTotal();
	public ItemBasic parseItemToBasic(Item item);
}