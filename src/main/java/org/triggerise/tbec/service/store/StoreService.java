package org.triggerise.tbec.service.store;

import java.util.List;

import org.triggerise.tbec.model.Item;

public interface StoreService {

	public String processSale(List<Item> toBuy);
}
