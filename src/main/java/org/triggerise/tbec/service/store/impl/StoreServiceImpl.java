package org.triggerise.tbec.service.store.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.triggerise.tbec.enumeration.item.ItemDiscountType;
import org.triggerise.tbec.model.Item;
import org.triggerise.tbec.service.item.ItemService;
import org.triggerise.tbec.service.store.StoreService;



@Service("StoreService")
public class StoreServiceImpl implements StoreService {
	@Autowired
	private ItemService itemService;

	@SuppressWarnings("unchecked")
	@Override
	public String processSale(List<Item> toBuy) {
		double totalToPay = 0.0;
		String itemsString = "";
		if(toBuy != null && !toBuy.isEmpty()){
			HashMap<Long, Integer> itemsQuantities = processListQuantities(toBuy);
			if(itemsQuantities != null){
				for ( Long key : itemsQuantities.keySet() ) {
					int quant = itemsQuantities.get(key);
					Item currentItem = itemService.getOneById(key);
					if(currentItem != null){
						if(currentItem.getIdt() == ItemDiscountType.TWO_FOR_ONE){
							totalToPay += (quant % 2 == 0) ? ((quant/2) * currentItem.getPrice()): ((quant == 1) ? (currentItem.getPrice()) : ((((quant-1)/2) * currentItem.getPrice()) + currentItem.getPrice()));
						}
						if(currentItem.getIdt() == ItemDiscountType.BULK){
							totalToPay += (quant >= 3 ) ? (quant * currentItem.getAltPrice()): (quant * currentItem.getPrice());
						}
						if(currentItem.getIdt() == ItemDiscountType.NORMAL){
							totalToPay += quant * currentItem.getPrice();
						}
					}
				}
			}
			itemsString = "Items: " + processListString(toBuy) + "\n Total: " + String.format( "%.2f", totalToPay ) + "�";
		}
		System.out.println(itemsString);
		return itemsString;
	}
	
	@SuppressWarnings("rawtypes")
	private HashMap processListQuantities(List<Item> diorganised){
		HashMap<Long, Integer> countItems = new HashMap<>();
		  for (Item item: diorganised) {
		      if (countItems.containsKey(item.getId()))
		    	  countItems.put(item.getId(), countItems.get(item.getId()) + 1);
		      else
		    	  countItems.put(item.getId(), 1);
		  }
		return countItems;
	}
	
	private String processListString(List<Item> list){
		String string = "";
		  for (Item item: list) {
			  string += item.getCode() + ", ";
		  }
		return string.substring(0, string.length()-2);
	}

}
